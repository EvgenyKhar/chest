﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Player : MonoBehaviour
{
    public float gold { get; protected set; } = 600f;
    public float armor { get; protected set; } = 15f;
    protected float maxArmor = 65;
    public List<Goods> inventory { get; protected set; }
    public int inventorySize { get; protected set; } = 2;
    public List<Chest> wearCells { get; protected set; }
    public abstract bool TryToBuy(Goods chest);
    public abstract bool TryToWear(Chest chest);
    public abstract bool TryToSell(Goods chest);
    public abstract bool TryToUnWear(Goods chest);
}
