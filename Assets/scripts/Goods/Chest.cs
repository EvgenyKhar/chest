﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Chest : Goods, ISellable, IWeareable, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField] public float armor;

    Transform oldTransform;
    Vector3 oldPosition;

    Camera mainCam;
    
    Transform canvas;
    Inventory inventory;
    Faceplate faceplate;
    Shop shop;
    
    ClientPlayer clientPlayer;



    void Start()
    {
        inventory = FindObjectOfType<Inventory>();
        faceplate = FindObjectOfType<Faceplate>();
        shop = FindObjectOfType<Shop>();
        canvas = FindObjectOfType<Canvas>().gameObject.transform;
        clientPlayer = FindObjectOfType<ClientPlayer>();
        mainCam = Camera.main;
        Physics.queriesHitTriggers = true;
        rectTransform = GetComponent<RectTransform>();
    }

    public bool TrySell(GameObject to)
    {
        if (to==inventory.GetGameObject())
        {
            if(clientPlayer.TryToBuy(this))
            {
                PutTo(inventory);
                return true;
            }
        }
        else
        {
            if(clientPlayer.TryToSell(this))
            {
                PutTo(shop);               
                return true;
            }
        }
      
        ReturnToOldPlace();
        return false;
    }

    private void ReturnToOldPlace()
    {
        transform.SetParent(oldTransform);
        transform.position = oldPosition;
    }

    public bool TryWear()
    {
        return clientPlayer.TryToWear(this);
    }

    public void PutTo(IPlaceable to)
    {
        oldTransform.GetComponent<IPlaceable>().Unplace(this);
        Vector3 newPosition = to.PlaceIn(this);
        
        if(newPosition != new Vector3(0,0,0))
        {
            transform.SetParent(to.GetGameObject().transform);
            transform.position = newPosition;
        }
        else
        {
            transform.position = oldTransform.GetComponent<IPlaceable>().PlaceIn(this);
            transform.SetParent(oldTransform);
            Debug.Log($"Can't place in {to}");
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        oldPosition = transform.position;
        oldTransform = transform.parent;
        transform.SetParent(canvas);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position=  mainCam.ScreenToWorldPoint( new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1 ));
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        
        RaycastHit hit;
       if(Physics.Raycast(Camera.main.transform.position, mainCam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1)), out hit, 1000f))
        {
            if(hit.transform.gameObject==oldTransform.gameObject)
            {
                ReturnToOldPlace();
            }
            else if ( hit.transform == shop.GetGameObject().transform && oldTransform!=faceplate.transform)
            {
                TrySell(hit.transform.gameObject);
            }
            else if(hit.transform == inventory.GetGameObject().transform)
            {
                if (clientPlayer.wearCells.Contains(this))
                {
                    TryUnwear();
                }
                else
                {
                    TrySell(hit.transform.gameObject);
                }
            }
            else if (hit.transform == faceplate.GetGameObject().transform && oldTransform!=shop.transform)
            {
                if(TryWear())
                {
                    PutTo(faceplate);
                }
            }
            else
            {
                ReturnToOldPlace();
            }
        }
        else
        {
            ReturnToOldPlace();
        }
    }

    public bool TryUnwear()
    {
        return clientPlayer.TryToUnWear(this);
    }
}
