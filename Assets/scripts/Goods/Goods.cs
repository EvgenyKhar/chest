﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Goods : MonoBehaviour
{
    public string description;
    [SerializeField] public float cost =100f;
    protected RectTransform rectTransform;
}
