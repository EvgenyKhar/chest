﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeareable 
{
    bool TryWear();
    bool TryUnwear();
}
