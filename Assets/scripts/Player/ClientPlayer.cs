﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClientPlayer : Player
{
    [SerializeField] GameObject progressBar;
    float progressBarFullSize;
    [SerializeField] Text goldText;
    ServerPlayer serverPlayer;
    void Start()
    {
        progressBarFullSize = progressBar.transform.localScale.x;
        inventory = new List<Goods>();
        wearCells = new List<Chest>();
        serverPlayer = FindObjectOfType<ServerPlayer>();
        UpdateState(armor, gold);
    }
    public override bool TryToBuy(Goods chest)
    {
        return serverPlayer.TryToBuy(chest);
    }

    public override bool TryToWear(Chest chest)
    {
        return serverPlayer.TryToWear(chest);
    }

    public override bool TryToSell(Goods chest)
    {
        return serverPlayer.TryToSell(chest);
    }

    public override bool TryToUnWear(Goods chest)
    {
        return serverPlayer.TryToUnWear(chest);
    }

    public void UpdateState(float armor, float gold )
    {
        this.armor = armor;
        this.gold = gold;
        goldText.text = gold.ToString();
        progressBar.transform.localScale = new Vector3(progressBarFullSize * armor / maxArmor, progressBar.transform.localScale.y, progressBar.transform.localScale.z);
    }

    public void UpdateInventory(Goods goods, bool needToAdd)
    {
        if(needToAdd)
        { inventory.Add(goods); }
        else
        { inventory.Remove(goods); }
    }

    public void UpdateWear(Chest chest, bool needToAdd)
    {
        if(needToAdd)
        { wearCells.Add(chest); }
        else
        { wearCells.Remove(chest); }
    }


}
