﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour, IPlaceable
{
    List<Goods> inventoryCells;
    [SerializeField] int inventoryCapacity =4;
    public Vector2 cellOffset;

    void Start()
    {
        inventoryCells = new List<Goods>();
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public Vector3 PlaceIn(Goods goods)
    {
        if (inventoryCells.Count < inventoryCapacity)
        {
            inventoryCells.Add(goods);
            return GetCellPosition(inventoryCells.Count-1);
        }
        else
        {
            return GetCellPosition(inventoryCapacity);
        }
    }

    public bool Unplace(Goods goods)
    {
        if(inventoryCells.Remove(goods))
        {
            foreach (Goods good in inventoryCells)
            {
                good.transform.position = GetCellPosition(inventoryCells.Count - 1);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    Vector3 GetCellPosition(int cellIndex)
    {
        cellOffset = transform.localScale*0.8f;

        switch(cellIndex)
        {
            case 0:
                return new Vector3(transform.position.x - cellOffset.x, transform.position.y + cellOffset.y, transform.position.z);
            case 1:
                return new Vector3(transform.position.x + cellOffset.x, transform.position.y + cellOffset.y, transform.position.z);
            case 2:
                return new Vector3(transform.position.x - cellOffset.x, transform.position.y - cellOffset.y, transform.position.z);
            case 3:
                return new Vector3(transform.position.x + cellOffset.x, transform.position.y - cellOffset.y, transform.position.z);
        }

        return new Vector3(0,0,0);
    }
}
