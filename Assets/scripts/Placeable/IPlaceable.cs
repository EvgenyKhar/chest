﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlaceable
{
    Vector3 PlaceIn(Goods goods);
    GameObject GetGameObject();
    bool Unplace(Goods goods);
}
