﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerPlayer : Player
{
    ClientPlayer clientPlayer;
    Inventory inventoryPlace;
    void Start()
    {
        inventoryPlace = FindObjectOfType<Inventory>();
        clientPlayer = FindObjectOfType<ClientPlayer>();
    }

    public override bool TryToBuy(Goods chest)
    {
        if(chest.cost<=gold && clientPlayer.inventory.Count < inventorySize)
        {
            gold -= chest.cost;
            clientPlayer.UpdateState(this.armor, this.gold);
            clientPlayer.UpdateInventory( chest, needToAdd: true);
            return true;
        }
        return false;
    }

    public override bool TryToWear(Chest chest)
    {
        
        if(clientPlayer.wearCells.Count>0)
        {
            TryToUnWear(clientPlayer.wearCells[0]);
        }
        armor += chest.armor;
        clientPlayer.UpdateState(this.armor, this.gold);
        clientPlayer.UpdateInventory(chest, needToAdd: false);
        clientPlayer.UpdateWear(chest,needToAdd: true);
        
        return true;
    }

    public override bool TryToUnWear(Goods chest)
    {
        if(clientPlayer.wearCells[0]==chest)
        {
            armor -= clientPlayer.wearCells[0].armor;
            clientPlayer.wearCells[0].PutTo(inventoryPlace);
            clientPlayer.UpdateInventory(clientPlayer.wearCells[0], needToAdd: true);
            clientPlayer.UpdateWear(clientPlayer.wearCells[0], needToAdd: false);
            clientPlayer.UpdateState(this.armor, this.gold);
            return true;
        }
        return false;
    }

    public override bool TryToSell(Goods chest)
    {
        gold += chest.cost;
        clientPlayer.UpdateState(this.armor, this.gold);
        clientPlayer.UpdateInventory(chest, needToAdd: false);
        return true;
    }

}
