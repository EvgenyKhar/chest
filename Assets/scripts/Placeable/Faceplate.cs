﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Faceplate : MonoBehaviour, IPlaceable
{
    List<Goods> facePlateCells;
    [SerializeField] int faceplateCapacity=1;
    Vector2 cellOffset;

    void Start()
    {
        facePlateCells = new List<Goods>();
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public Vector3 PlaceIn(Goods goods)
    {
        if(facePlateCells.Count<faceplateCapacity)
        {
            facePlateCells.Add(goods);
            return transform.position;
        }
        else
        {
            facePlateCells[faceplateCapacity - 1].gameObject.GetComponent<Chest>().PutTo(FindObjectOfType<Inventory>());
            Unplace(facePlateCells[faceplateCapacity - 1]);
            facePlateCells.Add(goods);
            return transform.position;
        }
    }

    public bool Unplace(Goods goods)
    {
        return facePlateCells.Remove(goods);
    }
}
