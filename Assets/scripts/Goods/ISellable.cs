﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISellable 
{
    bool TrySell(GameObject to);
}
