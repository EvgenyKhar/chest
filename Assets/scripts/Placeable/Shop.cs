﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour, IPlaceable
{

    List<Goods> shopCells;
    [SerializeField] int shopCapacity = 4;
    Vector2 cellOffset;

    void Start()
    {
        shopCells = new List<Goods>();
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public Vector3 PlaceIn(Goods goods)
    {
        if (shopCells.Count < shopCapacity)
        {
            shopCells.Add(goods);
            return GetCellPosition(shopCells.Count-1);
        }
        else
        {
            return GetCellPosition(shopCapacity);
        }
    }

    public bool Unplace(Goods goods)
    {
        if (shopCells.Remove(goods))
        {
            foreach (Goods good in shopCells)
            {
                good.transform.position = GetCellPosition(shopCells.Count - 1);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    Vector3 GetCellPosition(int cellIndex)
    {
        cellOffset = transform.localScale*0.8f;

        switch (cellIndex)
        {
            case 0:
                return new Vector3(transform.position.x - cellOffset.x, transform.position.y + cellOffset.y, transform.position.z);
            case 1:
                return new Vector3(transform.position.x + cellOffset.x, transform.position.y + cellOffset.y, transform.position.z);
            case 2:
                return new Vector3(transform.position.x - cellOffset.x, transform.position.y - cellOffset.y, transform.position.z);
            case 3:
                return new Vector3(transform.position.x + cellOffset.x, transform.position.y - cellOffset.y, transform.position.z);
        }

        return new Vector3(0, 0, 0);
    }

}
